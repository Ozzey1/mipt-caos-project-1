# Documentation

## Description 

- This is a C program that uses multiple threads to multiply two matrices of the same size, specified by the user.
- The program uses the pthread library to create and manage the threads. 
- The program starts by initializing a seed for the random number generator, then it reads the size of the matrices and the number of threads to use from the user input. It then creates three matrices of the specified size, fills them with random values, multiplies the first two matrices using the multiplyMatrix function and assigns the result to the third matrix, and finally displays the resulting matrix on the screen. 
- The multiplyMatrix function uses a global variable and a mutex to keep track of the next row to be filled and synchronize access to it among the threads. 
- The function creates the specified number of threads and assigns them the task of filling one row of the result matrix at a time using the rows of the first matrix and the columns of the second matrix.
- The function then waits for all threads to finish before returning.

## How to Use

1. Save the code file as matrix.c

2. Open terminal in the directory 

3. Compile the code using command:
    ```bash
        gcc -o matrix.out matrix.c
    ```

4. Get the output using command:
    ```bash
        .\matrix.out
    ```
