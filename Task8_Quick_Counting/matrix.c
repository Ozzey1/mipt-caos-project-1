#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

/*
    Struct to store the matrices and their size
*/
typedef struct {
    int n;
    bool *a;
    bool *b;
    bool *c;
} matrices_struct;

/*
    Global variable to keep track of the next row to be filled
    Mutex to synchronize access to this variable
*/
int freerow = 0;
pthread_mutex_t rowmutex = PTHREAD_MUTEX_INITIALIZER;

/*
    Function to fill a matrix of size n with random values
*/
void fillMatrix(int n, bool x[n][n]){
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            x[i][j] = rand() & 1;
        }
    }
}

/*
    Function to display a matrix of size n
*/
void showMatrix(int n, bool x[n][n]){
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            printf("%d ", x[i][j]);
        }
        printf("\n");
    }
}

/*
    Thread function to fill a row of the result matrix
    Each thread will fill one row of the matrix at a time
    using the rows of matrix a and the columns of matrix b
*/
void *fillRow(void *args){
    int i;
    matrices_struct *ms = args;
    while (freerow < ms->n){
        pthread_mutex_lock(&rowmutex);
        if(freerow >= ms->n){
            pthread_mutex_unlock(&rowmutex);
            break;
        }
        i = freerow;
        freerow++;
        pthread_mutex_unlock(&rowmutex);

        for(int j = 0; j < ms->n; j++){
            bool finish = false;
            for(int m = 0; m < ms->n; m++){
                finish = finish || (*(ms->a + i*ms->n + m) && *(ms->b + m*ms->n + j));
                if(finish)
                    break;
            }
            *(ms->c + i*ms->n + j) = finish;
        }
    }
}

/*
    Function to multiply two matrices using multiple threads
    Takes in the size of the matrices, number of threads to use,
    and pointers to the matrices a, b, and c
*/
void multiplyMatrix(int n, int numOfThreads, bool *a, bool *b, bool *c){
    pthread_t threads[numOfThreads];
    matrices_struct *ms = malloc(sizeof(*ms));
    ms->n = n;
    ms->a = a;
    ms->b = b;
    ms->c = c;

// Lock the mutex before creating the threads
    pthread_mutex_lock(&rowmutex);
    for(int i = 0; i < numOfThreads; i++){
        if ( pthread_create( &threads[i], NULL, fillRow, ms) ) {
            printf("Error creating thread.");
            abort();
        }
    }
// Unlock the mutex after creating the threads
    pthread_mutex_unlock(&rowmutex);

// Wait for all threads to finish
    for(int i = 0; i < numOfThreads; i++){
        if ( pthread_join ( threads[i], NULL ) ) {
            printf("Error joining thread.");
            abort();
        }
    }
}

int main() {
    srand(time(NULL));
    int size;
    int numOfThreads;
    scanf("%d", &size);
    scanf("%d", &numOfThreads);

    bool a[size][size];
    bool b[size][size];
    bool c[size][size];

    fillMatrix(size, a);
    fillMatrix(size, b);

    showMatrix(size, a);
    printf("\n");

    showMatrix(size, b);
    printf("\n");

    printf("\n");
    printf("\n");
    multiplyMatrix(size, numOfThreads, *a, *b, *c);
    printf("\n");
    printf("\n");

    showMatrix(size, c);
    printf("%d \n", freerow);
}
