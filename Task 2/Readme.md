# MIPT CAOS Task-2

This is a binary file reader program written in C Language

****************************************************
## Theory

We simply have to use ```fopen``` function to read the binary file

****************************************************
## Code explaination

* The binary data is stored in struct elem defined as:

```c
struct elem {
    int32_t value;
    uint32_t next;
};
```

* Then we are creating a pointer for the Binary file
```c
FILE *fptr
fptr = fopen("binary.bin","rb"))
```

* If it's a NULL pointer, the program will throw and Error

* If the file is ok, the program reads the contents of the .bin file and print it in a systematic order.
***************************************************
## Example output:

```c
Value: 100      Next: 9
Value: 81       Next: 8
Value: 64       Next: 7
Value: 49       Next: 6
Value: 36       Next: 5
Value: 25       Next: 4
Value: 16       Next: 3
Value: 9        Next: 2
Value: 4        Next: 1
Value: 1        Next: 0
```

******************************************************

*Note : Sample binary file is included in the directory*
