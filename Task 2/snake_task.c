#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Struct for storing the data
struct elem {
    int32_t value;
    uint32_t next;
};

int main() {
    //  Initialising the struct
    struct elem source;
    FILE *fptr;

    // Checking if the binary file is broken
    if ((fptr = fopen("binary.bin", "rb")) == NULL) {
        printf("Error!");

        // Exit code
        exit(1);
    }

    // Outputting the data until next == 0
    while (source.next) {
        fread(&source, sizeof(struct elem), 1, fptr);
        printf("Value: %d\tNext: %d\n", source.value, source.next);
    }
    // Closing the file
    fclose(fptr);

    return 0;
}
