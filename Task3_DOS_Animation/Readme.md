# Documentation

## Description 

- This code is written in assembly language for the x86 architecture. 
- It is setting up a data segment and initializing various variables. 
- The code then sets the resolution of the screen using the INT 10h interrupt, and clears the screen by writing to the video memory. - It also defines some procedures for drawing a rectangle and moving it based on input from the keyboard. The code uses various registers such as AX, CX, and DX to manipulate memory and perform computations. 
- The code also uses JE and JMP instructions to jump to different parts of the program depending on certain conditions.
- The animation contains a BLUE background and Cyan square block
- The block can be controlled using arrow keys

## How to Use

1. Save the code file as anim.asm

2. Download Dosbox and 8086 assembler and compiler (TASM & LINK) 

3. Assemble and Compile the code using command:
    ```bash
        TASM anim
        LINK anim
    ```

4. Run the program using:
    ```bash
        anim
    ```

## Output

![animat](https://user-images.githubusercontent.com/49760167/214176920-08c80df9-df7c-4988-860a-68282bee4d5d.gif)
