# Documentation

## Description 

- This is a Keylogger program written in Assembly (8086) supported by TASM and Linker.
- TSR (Terminate and Stay Resident) program written in assembly language for the x86 architecture. 
- It intercepts the keyboard interrupt (INT 09h) and records keystrokes to a buffer. 
- If the buffer overflows, the program calls the original interrupt handler. 
- The keystrokes are then written to a file named "c:\logger.txt" when the program receives interrupt 28h.

The 8086 assembler and compiler can be downloaded from [here](https://drive.google.com/file/d/1DoQrdZ6WJOGZwa-EuDXU-DnCHBtf2tl8/view)

## How to Use

1. Save the code file as key.asm

2. Download Dosbox and 8086 assembler and compiler (TASM & LINK) 

3. Assemble and Compile the code using command:
    ```bash
        TASM key
        LINK key
    ```

4. Run the program using:
    ```bash
        key
    ```



