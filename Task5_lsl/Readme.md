# Documentation

## Description 

The ls command lists all files in the directory that match the name. If name is left blank, it will list all of the files in the directory.

*ls -l* command lists contents of the directory with details.

## How to Use

1. Save the code file as ls.c

2. Open terminal in the directory 

3. Compile the code using command:
    ```bash
        gcc -o ls.out ls.c
    ```

4. Get the output for ls command using:
    ```bash
        .\ls.out
    ```
5. Get the output for **ls -l** command using:
    ```bash
        .\ls.out -l
    ```
