#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

void copy_file(const char* src, const char* dest)
{
    int src_fd = open(src, O_RDONLY);
    if (src_fd == -1)
    {
        fprintf(stderr, "Error opening file %s: %s\n", src, strerror(errno));
        return;
    }

    int dest_fd = open(dest, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (dest_fd == -1)
    {
        fprintf(stderr, "Error opening file %s: %s\n", dest, strerror(errno));
        close(src_fd);
        return;
    }

    char buf[4096];
    ssize_t bytes_read;
    while ((bytes_read = read(src_fd, buf, sizeof buf)) > 0)
    {
        if (write(dest_fd, buf, bytes_read) != bytes_read)
        {
            fprintf(stderr, "Error writing to file %s: %s\n", dest, strerror(errno));
            break;
        }
    }

    close(src_fd);
    close(dest_fd);
}

void copy_directory(const char* src, const char* dest)
{
    struct stat st = {0};
    if (stat(dest, &st) == -1) {
        // Create destination directory
        if (mkdir(dest, 0777) == -1)
        {
            fprintf(stderr, "Error creating directory %s: %s\n", dest, strerror(errno));
            return;
        }
    }

    DIR* dir = opendir(src);
    if (dir == NULL)
    {
        fprintf(stderr, "Error opening directory %s: %s\n", src, strerror(errno));
        return;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
        {
            continue;
        }

        char src_path[4096];
        snprintf(src_path, sizeof src_path, "%s/%s", src, entry->d_name);

        char dest_path[4096];
        snprintf(dest_path, sizeof dest_path, "%s/%s", dest, entry->d_name);

        // Check if the entry is a directory
                if (entry->d_type == DT_DIR)
        {
            copy_directory(src_path, dest_path);
        }
        else
        {
            copy_file(src_path, dest_path);
        }
    }

    closedir(dir);
}

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s source destination\n", argv[0]);
        return EXIT_FAILURE;
    }

    copy_directory(argv[1], argv[2]);

    return EXIT_SUCCESS;
}


