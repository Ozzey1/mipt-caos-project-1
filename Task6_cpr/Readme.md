# Documentation

## Description 

cp is a command in various Unix and Unix-like operating systems for copying files and directories.
-R or -r (recursive) – copy directories recursively

The file structure: 
![image](https://user-images.githubusercontent.com/49760167/214844248-719a974b-8d32-42bb-9cc3-ccc23bf276c3.png)

The program will copy all contents of *tst* to *dest*

## How to Use

1. Save the code file as ls.c

2. Open terminal in the directory 

3. Compile the code using command:
    ```bash
        gcc cp_r.c -o cp_r 
    ```

4. Get the output for ls command using:
    ```bash
        ./cp_r tst dest 
    ```
    Here 
        tst : Source file
        dest: Destination file
    

