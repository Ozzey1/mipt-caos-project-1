#include <stdio.h>
#include <stdint.h>
#include <limits.h>

// Parser function
void float_parser(float f) {
    union {
        float f;
        uint32_t u;
    }
    fu = {.f = f};
    int i = sizeof f * CHAR_BIT;

    printf("  ");
    while (i--)
        printf("%d ", (fu.u >> i) & 0x1);

//  Output template
    putchar('\n');
    printf(" |- - - - - - - - - - - - - - - - - - - - - - "
           "- - - - - - - - - -|\n");
    printf(" |s|      exp      |                  mantissa"
           "                   |\n\n");

}

// Driver code
int main(void) {

    // Declaring and taking in float value
    float f;
    scanf("%f", &f);

    // Outputting the binary representation
    printf("\nBitwise representation:\n\n");
    float_parser(f);
    return 0;
}
