# MIPT CAOS Task-1

This is float parser program written in C language

****************************************************
## Theory

We are simply trying to output the bits in memory that make up a `float`.
All x86 type implementation are stored in *IEEE-754 Single Precision Floating-Point Format*

****************************************************
## Code explaination

<limits.h> header file is used for the CHAR_BIT

Then the values are stored in a string and outputted in an annoted way for identification of sign, normalized exponent and mantissa.

***************************************************
## Example output:

```c
15.15

Bitwise representation:

  0 1 0 0 0 0 0 1 0 1 1 1 0 0 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0
 |- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
 |s|      exp      |                  mantissa                   |
```
